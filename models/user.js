import mongoose from "mongoose";
import validator from "validator";
import bcrypt from "bcryptjs";
import crypto from "crypto";

const userSchema = new mongoose.Schema({
    userName: {
        type: String,
        trim: true,
        required: [true, "Please enter your name"]
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, "Please provide valid email address"],
        required: [true, "Please enter your email"]
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, "Please confirm your password"],
        validate: {
            validator: function(el) {
                return el === this.password;
            }
        }
    },
    role: {
        type: String,
        enum: ["user", "admin", "manager"],
        default: "user"
    },
    passwordChangedAt: {
        type: Date,
        default: () => Date.now() - 7 * 24 * 60 * 60 * 1000
    },
    passwordResetToken: String,
    passwordResetExpiry: Date,
    created: {
        type: Date,
        default: Date.now
    },
    updated: { type: Date }
});

userSchema.pre("save", async function(next) {
    if (!this.isModified("password")) return next();
    this.password = await bcrypt.hash(this.password, 12);
    this.passwordConfirm = undefined;
    next();
});

// virtual field
// userSchema
//     .virtual("password")
//     .set(function(password) {
//         // create temporary variable called _password
//         this._password = password;
//         // generate a timestamp
//         this.salt = uuidv1();
//         // encryptPassword()
//         this.hashedPassword = this.encryptPassword(password);
//     })
//     .get(function() {
//         return this._password;
//     });

// methods
userSchema.methods.correctPassword = async function(hashedPass, plainPass) {
    console.log(await bcrypt.compare(plainPass, hashedPass), "ssssssss");
    return await bcrypt.compare(plainPass, hashedPass);
};

userSchema.methods.isPasswordChangedAfterSignIn = function(JWTTimeStamp) {
    if (this.passwordChangedAt) {
        console.log(this.passwordChangedAt.getTime() / 1000, JWTTimeStamp);
        const changedPassTimeStamp = parseInt(
            this.passwordChangedAt.getTime() / 1000,
            10
        );
        return JWTTimeStamp < changedPassTimeStamp; // 10000 < 40000
    }

    return false;
};

userSchema.methods.generatePasswordToken = function() {
    const resetToken = crypto.randomBytes(32).toString("hex");

    this.passwordResetToken = crypto
        .createHash("sha256")
        .update(resetToken)
        .digest("hex");
    this.passwordResetExpiry = Date.now() + 10 * 60 * 1000;

    console.log(passwordResetToken, "passwordResetToken");
    console.log(passwordResetExpiry, "passwordResetExpiry");
    return resetToken;
};

// pre middleware
// userSchema.pre("remove", function(next) {
//     Post.remove({ postedBy: this._id }).exec();
//     next();
// });

module.exports = mongoose.model("User", userSchema);
