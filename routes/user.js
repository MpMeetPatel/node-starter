import expressPromiseRouter from "express-promise-router";
import {
    signInUser,
    signUpUser,
    authenticateUser,
    allowPermissionTo,
    forgotPassword,
    resetPassword
} from "../controller/user";

const router = expressPromiseRouter();

router.post("/signup", signUpUser);
router.post("/signin", signInUser);

router.post("/forgotPassword", forgotPassword);
router.post("/resetPassword", resetPassword);


router.get(
    "/private/manager",
    authenticateUser,
    allowPermissionTo("manager", "admin"),
    (req, res) => {
        console.log("Welcome to private world of manager and admin !!!");
        return res.send({
            mag: "Welcome to private world of manager and admin!!!"
        });
    }
);

router.get(
    "/private/user",
    authenticateUser,
    allowPermissionTo("user", "admin"),
    (req, res) => {
        console.log("Welcome to private world of user and admin !!!");
        return res.send({
            mag: "Welcome to private world of user and admin!!!"
        });
    }
);

router.param("userId", (req, res, next, id) => {
    res.send({ id: id });
});

export { router as userRoutes };
