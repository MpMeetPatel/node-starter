import User from "../models/user";
import bcrypt from "bcryptjs";
import multer from "multer";
import jwt from "jsonwebtoken";
import { promisify } from "util";
import nodemailer from "nodemailer";

export const signUpUser = async (req, res) => {
    try {
        const newUser = await User.create({
            email: req.body.email,
            userName: req.body.userName,
            password: req.body.password,
            passwordConfirm: req.body.passwordConfirm
        });

        res.status(201).send({
            status: "success",
            data: {
                user: newUser
            }
        });
    } catch (error) {
        console.log("error", error);
    }
};

export const updateSignedInUserProfile = async (req, res) => {};

export const signInUser = async (req, res) => {
    try {
        const { email, password } = req.body;
        console.log(req.body);
        if (!email || !password) {
            return res
                .status(400)
                .send({ error: "Please enter email and password" });
        }

        const user = await User.findOne({ email }).select("+password");

        if (!user || !(await user.correctPassword(user.password, password))) {
            return res
                .status(401)
                .send({ error: "Incorrect email or password" });
        }

        const token = await jwt.sign({ id: user._id }, process.env.SECRET, {
            expiresIn: "90d"
        });

        return res.status(200).send({
            status: "success",
            token
        });
    } catch (error) {
        console.log("error", error);
    }
};

export const authenticateUser = async (req, res, next) => {
    // 1) check authToken is provided in header
    const token = req.headers.authorization.split(" ")[1];
    if (!token) {
        return res.status(401).send({
            error: "you're not logged in, please login !"
        });
    }

    // 2) varify token
    const decoded = await promisify(jwt.verify)(token, process.env.SECRET);

    // 3) Check user still exists
    const currentUser = await User.findById(decoded.id);
    if (!currentUser) {
        return res.status(401).send({
            error: "token expired or user doesn't exists"
        });
    }

    // 4) check password is changes after

    if (currentUser.isPasswordChangedAfterSignIn(decoded.iat)) {
        return res.status(400).send({
            error: "you've changed password !"
        });
    }

    req.user = currentUser;

    next();
};

export const allowPermissionTo = (...roles) => {
    // roles = ["user","manager","admin"] , role="user" || "manager" || "admin"
    // roles = ["user","manager"] , role="user" || "manager"
    return (req, res, next) => {
        if (!roles.includes(req.user.role)) {
            return res.status(403).send({
                error: "user don't have permision to perform this task"
            });
        }
        next();
    };
};

export const forgotPassword = async (req, res, next) => {
    // 1) get user based on email
    const user = await User.findOne({ email: req.body.email });

    if (!user) {
        return res.status(404).send({ error: "User Not Found" });
    }

    // 2) generate passwordReset Token
    const resetToken = user.generatePasswordToken();
    await user.save({ validateBeforeSave: false });

    // 3) send it to use via mail
    const resetURL = `${req.protocol}://${req.get(
        "host"
    )}/api/v1/user/reserPasword/${resetToken}`;

    try {
        await sendEmail({
            email: user.email,
            subject: "reset password(in 10 min  )",
            message: user.subject
        });
    } catch (error) {
        user.passwordResetToken = undefined;
        user.passwordResetExpiry = undefined;
        await user.save({ validateBeforeSave: false });
        return res.status(500).send({ msg: "Internal server error" });
    }

    res.send({ msg: "Token send to email !" });
};

export const resetPassword = async () => {};

export const sendEmail = async options => {
    // 1) create transporter
    const transporter = nodemailer.createTransport({
        service: "Gmail",
        host: "smtp.mailtrap.io",
        host: 25,
        auth: {
            user: "ddfdd8fb93c94f",
            pass: "c8809ead1fa34e"
        }
        // if gmail as a services
        // activate less secure app in gmail act
    });

    // 2) define mail options
    const mailOptions = {
        from: "Meet Patel <meet@pro.io>",
        to: options.email,
        subject: options.subject,
        text: options.message
        // html: ""
    };
    // 3) send the mail
    await transporter.sendMail(mailOptions);
};

export const signOutUser = async (req, res) => {};

export const getUserById = async (req, res, next, id) => {};

export const getAllUsers = async (req, res) => {};
