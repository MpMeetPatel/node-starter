import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import { userRoutes } from "../routes/user";

dotenv.config();
const app = express();

mongoose
    .connect(process.env.MONGO_URI, { useNewUrlParser: true, useCreateIndex: true })
    .then(() => {
        console.log("MongoDB Connected");
    })
    .catch(err => {
        throw err;
    });

// parse application/json
app.use(bodyParser.json());
// cookie middleware
app.use(cookieParser());

app.use("/api/v1/user", userRoutes);

app.use(function(err, req, res, next) {
    if (err.name === "UnauthorizedError") {
        res.status(err.status).send({ message: err.message });
        return;
    }
    next();
});

export default app;
